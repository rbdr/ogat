Class(Ogat.Nodes, "Gravity").inherits(Serpentity.Node)({
    types : {
        position : Ogat.Components.Position,
        motion : Ogat.Components.Motion,
        weight : Ogat.Components.Weight,
    }
});
