Class(Ogat.Nodes, "Eye").inherits(Serpentity.Node)({
    types : {
        eye : Ogat.Components.Eye,
        follower : Ogat.Components.Follower,
        position : Ogat.Components.Position
    }
});
