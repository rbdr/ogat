Class(Ogat.Nodes, "Collider").inherits(Serpentity.Node)({
    types : {
        position : Ogat.Components.Position,
        collider : Ogat.Components.Collider,
        motion : Ogat.Components.Motion
    }
});
