Class(Ogat.Nodes, "PolygonRender").inherits(Serpentity.Node)({
    types : {
        position : Ogat.Components.Position,
        bounds : Ogat.Components.Bounds,
        polygons : Ogat.Components.Polygons
    }
});

