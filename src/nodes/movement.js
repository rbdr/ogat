Class(Ogat.Nodes, "Movement").inherits(Serpentity.Node)({
    types : {
        position : Ogat.Components.Position,
        motion : Ogat.Components.Motion,
        bounds : Ogat.Components.Bounds
    }
});
