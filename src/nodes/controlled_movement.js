Class(Ogat.Nodes, "ControlledMovement").inherits(Serpentity.Node)({
    types : {
        position : Ogat.Components.Position,
        motion : Ogat.Components.Motion,
        bounds : Ogat.Components.Bounds,
        jump : Ogat.Components.Jump,
        attack : Ogat.Components.Attack,
        collider : Ogat.Components.Collider,
        motion_controls : Ogat.Components.MotionControls
    }
});
