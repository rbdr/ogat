Module(Ogat, "EntityFactory")({
    // Will create an entity that can be drawn and moved
    createSampleEntity : function createSampleEntity() {
        var entity, graphic;
            entity = new (Serpentity.Entity)();
            entity.addComponent(new Ogat.Components.Position({
                x: 100,
                y: 100
            }));
            entity.addComponent(new Ogat.Components.Motion());
            entity.addComponent(new Ogat.Components.MotionControls({
                left: Phaser.Keyboard.LEFT,
                right: Phaser.Keyboard.RIGHT,
                jump: Phaser.Keyboard.X,
                attack: Phaser.Keyboard.Z,
                accelerationRate : 4,
                maxVelocity : {
                  x: 3,
                  y: 7
                }
            }));
            entity.addComponent(new Ogat.Components.Weight());
            entity.addComponent(new Ogat.Components.Jump({
              jumps : 2
            }));
            entity.addComponent(new Ogat.Components.Attack());

            // This is the main bounding box
            entity.addComponent(new Ogat.Components.Bounds({
              w: 16,
              h: 32
            }));

            // These are the polygons to draw
            entity.addComponent(new Ogat.Components.Polygons({
              polygons : [
                {
                  radius: 16,
                  rotation: Math.PI / 4,
                  precision: 4,
                  stretch: {
                    x: 0.5,
                    y: 1
                  }
                }
              ]
            }));

            entity.addComponent(new Ogat.Components.Collider({
              x: -8,
              y: 0,
              w: 32,
              h: 32,
              type: "good"
            }))


            Ogat.engine.addEntity(entity);
            return entity;
    },
    createEye : function createEye(player) {
        var entity, graphic;
            entity = new (Serpentity.Entity)();
            entity.addComponent(new Ogat.Components.Position({
                x: 103,
                y: 100
            }));

            // This is the main bounding box
            entity.addComponent(new Ogat.Components.Bounds({
              w: 50,
              h: 20
            }));

            var eyeball = {
              radius: 16,
              precision: 10,
              color: "red",
              fill: true
            };
            var pupil = {
              radius: 11,
              precision: 10,
              fill: true,
              color: "black"
            };

            // These are the polygons to draw
            entity.addComponent(new Ogat.Components.Polygons({
              polygons : [
                {
                  radius: 50,
                  precision: 10,
                  fill: true,
                  stretch: {
                    x: 1,
                    y: 0.4
                  }
                },
                eyeball,
                pupil
              ]
            }));

            entity.addComponent(new Ogat.Components.Eye({
              eyeball: eyeball,
              pupil: pupil
            }))

            // This is the main bounding box
            entity.addComponent(new Ogat.Components.Follower({
              follower: player
            }));

            return Ogat.engine.addEntity(entity);
    },

    createRhombus : function (x, y) {
      var entity;

      entity = new (Serpentity.Entity)();
      entity.addComponent(new Ogat.Components.Position({
          x: x,
          y: y
      }));
      entity.addComponent(new Ogat.Components.Motion({
        bounce: true,
        deccelerationRate: 2,
        damping: 1.1,
      }));

      // This is the main bounding box
      entity.addComponent(new Ogat.Components.Bounds({
        w: 8,
        h: 8
      }));

      // These are the polygons to draw
      entity.addComponent(new Ogat.Components.Polygons({
        polygons : [
          {
            radius: 8,
            precision: 4,
            fill: true,
            color : "rgb(" + Math.floor(Math.random() * 255) + "," + Math.floor(Math.random() * 255) + "," + Math.floor(Math.random() * 255) + ")"
          }
        ]
      }));

      entity.addComponent(new Ogat.Components.Collider({
        x: -4,
        y: -4,
        w: 16,
        h: 16,
        active: true
      }))

      // TODO: Make a random behavior :3
      entity.addComponent(new Ogat.Components.Weight({}));

      Ogat.engine.addEntity(entity);
      return entity;
    }
});
