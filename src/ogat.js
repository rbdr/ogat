Module("Ogat")({
    Components : {},
    Systems : {},
    Nodes : {},
    engine : null,
    game : null,
    scale : 1,
    resolution : {
      w: 256,
      h: 240
    },
    init : function init(config) {

      this.engine = new Serpentity();
      this.entityFactory = Ogat.EntityFactory;
      this.game = new Phaser.Game(this.resolution.w * this.scale, this.resolution.h * this.scale, Phaser.AUTO, '', {
          preload : this._preload.bind(this),
          create  : this._create.bind(this),
          update  : this._update.bind(this)
      }, false);
      this.game.antialias = false;
    },

    _preload : function preload() {
        this._initializeSystems();
        this._initializeEntities();
        this.game.camera.scale.setTo(this.scale, this.scale);
        console.log("Preloading.");
    },

    _create : function create() {
        console.log("Creating.");
    },

    _update : function update(game) {
        this.engine.update(game.time.elapsed);
    },

    _initializeSystems : function initializeSystems() {
        this.engine.addSystem(new Ogat.Systems.MainControl({
            keyboard : this.game.input.keyboard,
        }));
        //this.engine.addSystem(new Ogat.Systems.PhaserRenderer());
        this.engine.addSystem(new Ogat.Systems.Acceleration());
        this.engine.addSystem(new Ogat.Systems.Gravity());
        this.engine.addSystem(new Ogat.Systems.Motion());
        this.engine.addSystem(new Ogat.Systems.PolygonRenderer());
        this.engine.addSystem(new Ogat.Systems.Follower());
        this.engine.addSystem(new Ogat.Systems.ColliderRenderer());
        this.engine.addSystem(new Ogat.Systems.Collider());
    },

    _initializeEntities : function initializeEntities() {
        var player;
        player = this.entityFactory.createSampleEntity();
        this.entityFactory.createEye(player);
        this.entityFactory.createRhombus(124, 150);
        this.entityFactory.createRhombus(116, 160);
        this.entityFactory.createRhombus(132, 160);
        this.entityFactory.createRhombus(124, 170);
    }
});
