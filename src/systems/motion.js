Class(Ogat.Systems, "Motion").inherits(Serpentity.System)({
  prototype : {
    keyboard : null,
    movables : null,
    init : function init(config) {
        var property;

        for (property in config) {
            if (config.hasOwnProperty(property)) {
                this[property] = config[property];
            }
        }
    },
    added : function added(engine) {
        this.movables = engine.getNodes(Ogat.Nodes.Movement);
    },
    removed : function removed(engine) {
        this.movables = null;
    },
    update : function update(dt) {
        this.movables.forEach(function (movable) {
            // Calculate positions.
            movable.position.y += movable.motion.velocity.y;
            movable.position.x += movable.motion.velocity.x;

            // Enforce bounds
            if (movable.position.y < 0) {
              movable.position.y = 0;
              if (movable.motion.velocity.y < 0) {
                if (movable.motion.bounce) {
                  movable.motion.velocity.y = -(movable.motion.velocity.y / movable.motion.damping); 
                } else {
                  movable.motion.velocity.y = 0;
                }
              }
            }

            if (movable.position.x < 0) {
              movable.position.x = 0;
              if (movable.motion.velocity.x < 0) {
                if (movable.motion.bounce) {
                  movable.motion.velocity.x = -(movable.motion.velocity.x / movable.motion.damping);
                } else {
                  movable.motion.velocity.x = 0;
                }
              }
            }

            if (movable.position.y > Ogat.resolution.h - movable.bounds.h) {
              movable.position.y = Ogat.resolution.h - movable.bounds.h;
              if (movable.motion.velocity.y > 0) {
                if (movable.motion.bounce) {
                  movable.motion.velocity.y = -(movable.motion.velocity.y / movable.motion.damping);
                } else {
                  movable.motion.velocity.y = 0;
                }
              }
            }

            if (movable.position.x > Ogat.resolution.w - movable.bounds.w) {
              movable.position.x = Ogat.resolution.w - movable.bounds.w;
              if (movable.motion.velocity.x > 0) {
                if (movable.motion.bounce) {
                  movable.motion.velocity.x = -(movable.motion.velocity.x / movable.motion.damping);
                } else {
                  movable.motion.velocity.x = 0;
                }
              }
            }
        }.bind(this));
    },
  }
});

