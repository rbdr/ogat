Class(Ogat.Systems, "PolygonRenderer").inherits(Serpentity.System)({
  prototype : {
    movables : null,
    defaults : null,
    bmd : null,
    init : function init(config) {
        var property;

        this.defaults = {
          radius : 10,
          precision : 10,
          color : "white",
          fill : false,
          rotation : 0,
          stretch : {
            x: 1,
            y: 1
          },
          position : {
            x: 0,
            y: 0
          }
        }

        for (property in config) {
            if (config.hasOwnProperty(property)) {
                this[property] = config[property];
            }
        }
    },
    added : function added(engine) {
      this.movables = engine.getNodes(Ogat.Nodes.PolygonRender);

      this.bmd = Ogat.game.add.bitmapData(Ogat.resolution.w, Ogat.resolution.h);
      Ogat.game.add.sprite(0, 0, this.bmd); 
    },
    removed : function removed(engine) {
        this.movables = null;
    },
    update : function update(dt) {
      this.bmd.clear();
      this.movables.forEach(function (movable) {
        movable.polygons.polygons.forEach(function (polygon) {
          var normalizedPolygon

          normalizedPolygon = {
            radius : polygon.radius || this.defaults.radius,
            precision : polygon.precision || this.defaults.precision,
            rotation : polygon.rotation || this.defaults.rotation,
            stretch : polygon.stretch || this.defaults.stretch,
            position : polygon.position || this.defaults.position,
            color : polygon.color || this.defaults.color,
            parentPosition : movable.position,
            bounds : movable.bounds,
            fill : polygon.fill || false
          }

          this._draw(normalizedPolygon);
        }.bind(this));
      }.bind(this));
    },

    _draw : function drawPolygon(polygon) {
      var intervalSize, angle, ox, oy;

      intervalSize = 2 * Math.PI / polygon.precision;

      ox = polygon.position.x + polygon.parentPosition.x + polygon.bounds.w / 2;
      oy = polygon.position.y + polygon.parentPosition.y + polygon.bounds.h / 2;

      this.bmd.ctx.beginPath();
      this.bmd.ctx.strokeStyle = polygon.color;
      this.bmd.ctx.fillStyle = polygon.color;
      this.bmd.ctx.moveTo(ox + polygon.radius * Math.cos(polygon.rotation) * polygon.stretch.x,
                          oy + polygon.radius * Math.sin(polygon.rotation) * polygon.stretch.y);

      for (angle = 0; angle < 2 * Math.PI; angle += intervalSize) {
          var y = polygon.radius * Math.sin(angle + polygon.rotation) * polygon.stretch.y;
          var x = polygon.radius * Math.cos(angle + polygon.rotation) * polygon.stretch.x;
          this.bmd.ctx.lineTo(ox + x, oy + y);
      }

      this.bmd.ctx.lineTo(ox + polygon.radius * Math.cos(polygon.rotation) * polygon.stretch.x,
                          oy + polygon.radius * Math.sin(polygon.rotation) * polygon.stretch.y);

      this.bmd.ctx.stroke();

      if (polygon.fill) {
          this.bmd.ctx.fill();
      }
    }
  }
});


