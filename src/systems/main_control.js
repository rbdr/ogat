Class(Ogat.Systems, "MainControl").inherits(Serpentity.System)({
    prototype : {
        keyboard : null,
        renderables : null,
        _maxJumpTime : 100,
        _maxAttackTime : 100,
        _jumpLock : false,
        init : function init(config) {
            var property;

            for (property in config) {
                if (config.hasOwnProperty(property)) {
                    this[property] = config[property];
                }
            }
        },
        added : function added(engine) {
            this.renderables = engine.getNodes(Ogat.Nodes.ControlledMovement);
        },
        removed : function removed(engine) {
            this.renderables = null;
        },
        update : function update(dt) {
            this.renderables.forEach(function (renderable) {
                var controlPressed;

                controlPressed = false;

                if (this.keyboard.isDown(renderable.motion_controls.left)) {
                  renderable.motion.acceleration.x = -renderable.motion_controls.accelerationRate;
                  controlPressed = true;
                }

                if (this.keyboard.isDown(renderable.motion_controls.right)) {
                  renderable.motion.acceleration.x = renderable.motion_controls.accelerationRate;
                  controlPressed = true;
                }

                if (!controlPressed) {
                  renderable.motion.acceleration.x = 0;
                }

                // ATTACK
                if (this.keyboard.isDown(renderable.motion_controls.attack)) {
                  if (!renderable.attack.lock) {
                    renderable.attack.lock = true;
                    renderable.collider.active = true;
                    renderable.attack.time = 0;
                  }
                } else {
                  if (renderable.attack.time > this._maxAttackTime) {
                    renderable.attack.lock = false;
                  }
                }

                if (renderable.attack.lock) {
                  renderable.attack.time += dt;
                }

                if (renderable.attack.time > this._maxAttackTime) {
                  renderable.collider.active = false;
                }

                // JUMP
                if (this.keyboard.isDown(renderable.motion_controls.jump)) {
                  if (!renderable.jump.jumpLock && renderable.position.y === Ogat.resolution.h - renderable.bounds.h) {
                    renderable.jump.jumpLock = true;
                    renderable.jump.multiJumpLock = true;
                    renderable.jump.rejumpLock = false;
                    renderable.jump.jumpTime = 0;
                    renderable.jump.currentJumps++;
                  }

                  if (!renderable.jump.multiJumpLock && renderable.jump.currentJumps < renderable.jump.jumps) {
                    renderable.jump.multiJumpLock = true;
                    renderable.jump.jumpTime = 0;
                    renderable.jump.currentJumps++;
                    renderable.motion.velocity.y = -renderable.jump.jumpStrength / 10;
                  }

                  if (!renderable.jump.rejumpLock && renderable.jump.jumpTime < this._maxJumpTime) {
                    renderable.jump.jumpTime += dt;
                    renderable.motion.acceleration.y = -renderable.jump.jumpStrength;
                  }
                } else {
                  if (renderable.jump.jumpLock && renderable.position.y === Ogat.resolution.h - renderable.bounds.h) {
                    renderable.jump.jumpLock = false;
                    renderable.jump.currentJumps = 0;
                  }
                  renderable.jump.rejumpLock = true;
                  renderable.jump.multiJumpLock = false;
                }
            }.bind(this));
        }
    }
});
