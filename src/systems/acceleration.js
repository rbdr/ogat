Class(Ogat.Systems, "Acceleration").inherits(Serpentity.System)({
  prototype : {
    keyboard : null,
    movables : null,
    init : function init(config) {
        var property;

        for (property in config) {
            if (config.hasOwnProperty(property)) {
                this[property] = config[property];
            }
        }
    },
    added : function added(engine) {
        this.movables = engine.getNodes(Ogat.Nodes.Movement);
    },
    removed : function removed(engine) {
        this.movables = null;
    },
    update : function update(dt) {
        this.movables.forEach(function (movable) {

            // Calculate new velocities
            this._accelerate('y', dt, movable);
            this._accelerate('x', dt, movable);
        }.bind(this));
    },

    _accelerate : function accelerate(axis, dt, movable) {
      var currentAcceleration, currentVelocity;

      currentAcceleration = movable.motion.acceleration[axis];
      currentVelocity = movable.motion.velocity[axis];
      maxVelocity = movable.motion.maxVelocity[axis]

      // Calculate velocity
      if (currentAcceleration !== 0 ) {
        movable.motion.velocity[axis] = currentVelocity + currentAcceleration * (dt / 1000);
        currentVelocity = movable.motion.velocity[axis];

        // Ensure maxVelocity is respected
        if (currentVelocity > maxVelocity) {
          movable.motion.velocity[axis] = maxVelocity;
        }

        if (currentVelocity < -maxVelocity) {
          movable.motion.velocity[axis] = -maxVelocity;
        }
      } else {
        // Deccelerate, ensure velocity is 0 if we over-deccelerate

        if (currentVelocity === 0) {
          return;
        }

        if (currentVelocity > 0) {
          movable.motion.velocity[axis] = currentVelocity - movable.motion.deccelerationRate * (dt / 1000.0);
          currentVelocity = movable.motion.velocity[axis];
          if (currentVelocity < 0) {
            movable.motion.velocity[axis] = 0;
          }
        } else {
          movable.motion.velocity[axis] = currentVelocity + movable.motion.deccelerationRate * (dt / 1000.0);
          currentVelocity = movable.motion.velocity[axis];
          if (currentVelocity > 0) {
            movable.motion.velocity[axis] = 0;
          }
        }
      }
    }
  }
});

