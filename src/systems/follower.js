Class(Ogat.Systems, "Follower").inherits(Serpentity.System)({
  prototype : {
    movables : null,
    init : function init(config) {
        var property;

        for (property in config) {
            if (config.hasOwnProperty(property)) {
                this[property] = config[property];
            }
        }
    },
    added : function added(engine) {
        this.movables = engine.getNodes(Ogat.Nodes.Eye);
    },
    removed : function removed(engine) {
        this.movables = null;
    },
    update : function update(dt) {
      this.movables.forEach(function (movable) {
        var followerPosition, x, y;

        followerPosition = movable.follower.follower.getComponent(Ogat.Components.Position);

        if (followerPosition) {
          x = followerPosition.x - movable.position.x;
          y = followerPosition.y - movable.position.y;
        }

        // Get relative to radius
        x = x / 50;
        y = y / 50;

        if (x > 1) {x = 1;}
        if (x < -1) {x = -1;}
        if (y > 1) {y = 1;}
        if (y < -1) {y = -1;}

        movable.eye.pupil.position = {
          x: x * 5,
          y: y * 5
        }

        movable.eye.eyeball.position = {
          x: x * 5,
          y: y * 5
        }
      }.bind(this));
    }
  }
});
