Class(Ogat.Systems, "Gravity").inherits(Serpentity.System)({
  prototype : {
    movables : null,
    gravity : 9.8,
    init : function init(config) {
        var property;

        for (property in config) {
            if (config.hasOwnProperty(property)) {
                this[property] = config[property];
            }
        }
    },
    added : function added(engine) {
        this.movables = engine.getNodes(Ogat.Nodes.Gravity);
    },
    removed : function removed(engine) {
        this.movables = null;
    },
    update : function update(dt) {
      this.movables.forEach(function (movable) {
        movable.motion.acceleration.y = this.gravity;
      }.bind(this));
    }
  }
});


