Class(Ogat.Systems, "ColliderRenderer").inherits(Serpentity.System)({
  prototype : {
    movables : null,
    defaults : null,
    bmd : null,
    init : function init(config) {
        var property;

        this.defaults = {
          radius : 10,
          precision : 10,
          rotation : 0,
          color : "white",
          fill : false,
          stretch : {
            x: 1,
            y: 1
          },
          position : {
            x: 0,
            y: 0
          }
        }

        for (property in config) {
            if (config.hasOwnProperty(property)) {
                this[property] = config[property];
            }
        }
    },
    added : function added(engine) {
      this.movables = engine.getNodes(Ogat.Nodes.Collider);

      this.bmd = Ogat.game.add.bitmapData(Ogat.resolution.w, Ogat.resolution.h);
      Ogat.game.add.sprite(0, 0, this.bmd); 
    },
    removed : function removed(engine) {
        this.movables = null;
    },
    update : function update(dt) {
      return;
      this.bmd.clear();
      this.movables.forEach(function (movable) {
        this._draw(movable);
      }.bind(this));
    },

    _draw : function drawPolygon(movable) {
      this.bmd.ctx.beginPath();
      if (movable.collider.active) {
        this.bmd.ctx.fillStyle = "rgba(0,255,0,0.5)";
      } else {
        this.bmd.ctx.fillStyle = "rgba(255,0,0,0.5)";
      }
      this.bmd.ctx.fillRect(movable.position.x + movable.collider.x,
                            movable.position.y + movable.collider.y,
                            movable.collider.w,
                            movable.collider.h
                           );
      this.bmd.ctx.fill();
    }
  }
});


