Class(Ogat.Systems, "Collider").inherits(Serpentity.System)({
  prototype : {
    force : 10,
    randomFactor : Math.PI/20,
    movables : null,
    init : function init(config) {
        var property;
    },
    added : function added(engine) {
      this.movables = engine.getNodes(Ogat.Nodes.Collider);
      Ogat.game.add.sprite(0, 0, this.bmd); 
    },
    removed : function removed(engine) {
        this.movables = null;
    },
    update : function update(dt) {
      var goodGuy = null;

      this.movables.some(function (element, index) {
        var segmentAX, segmentAY;

        if (element.collider.type === "good") {
          goodGuy = index;
          return true;
        }
        return false;
      });

      if (goodGuy !== null) {
        goodGuy = this.movables[goodGuy];

        segmentAX = [goodGuy.position.x + goodGuy.collider.x,
                     goodGuy.position.x + goodGuy.collider.x + goodGuy.collider.w];
        segmentAY = [goodGuy.position.y + goodGuy.collider.y,
                     goodGuy.position.y + goodGuy.collider.y + goodGuy.collider.h];

        this.movables.forEach(function (movable) {
          var segmentBX, segmentBY, dx, dy, angle, fx, fy;

          if (goodGuy.collider.active &&
              movable.collider.active && 
              movable.collider.type !== goodGuy.collider.type) {

            segmentBX = [movable.position.x + movable.collider.x,
                         movable.position.x + movable.collider.x + movable.collider.w];
            segmentBY = [movable.position.y + movable.collider.y,
                         movable.position.y + movable.collider.y + movable.collider.h];

            if (movable.collider.type === "good") {
                return;
            }

            if (segmentAX[0] < segmentBX[1] &&
                segmentAX[1] > segmentBX[0] &&
                segmentAY[0] < segmentBY[1] &&
                segmentAY[1] > segmentAY[0]) {

              dx = (segmentAX[0] + segmentAX[1] - segmentAX[0]) -
                    (segmentBX[0] + segmentBX[1] - segmentBX[0]);
              dy = (segmentAY[0] + segmentAY[1] - segmentAY[0]) -
                    (segmentBY[0] + segmentBY[1] - segmentBY[0]);

              angle = Math.atan2(-dy, -dx);

              angle += -this.randomFactor / 2 + Math.random() * this.randomFactor;

              fx = this.force * Math.cos(angle);
              fy = this.force * Math.sin(angle);

              movable.motion.velocity.x = fx;
              movable.motion.velocity.y = fy;
            }
          }
        }.bind(this));
      }
    },

    _draw : function drawPolygon(movable) {
      this.bmd.ctx.beginPath();
      if (movable.collider.active) {
        this.bmd.ctx.fillStyle = "rgba(0,255,0,0.5)";
      } else {
        this.bmd.ctx.fillStyle = "rgba(255,0,0,0.5)";
      }
      this.bmd.ctx.fillRect(movable.position.x + movable.collider.x,
                            movable.position.y + movable.collider.y,
                            movable.collider.w,
                            movable.collider.h
                           );
      this.bmd.ctx.fill();
    }
  }
});


