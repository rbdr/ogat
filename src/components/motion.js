Class(Ogat.Components, "Motion")({
    prototype : {
        acceleration : null,
        velocity : null,
        maxVelocity: null,
        bounce: false,
        deccelerationRate : 5,
        angularVelocity : 0,
        damping : 1,

        init : function (config) {
            var property;

            config = config || {};

            this.velocity = {
                x : 0,
                y : 0
            }
            this.acceleration = {
                x : 0,
                y : 0
            }
            this.maxVelocity = {
                x : 2,
                y : 7
            }

            for (property in config) {
                if (config.hasOwnProperty(property)) {
                    this[property] = config[property];
                }
            }
        }
    }
});
