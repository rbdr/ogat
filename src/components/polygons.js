Class(Ogat.Components, "Polygons").inherits(Serpentity.Component)({
    prototype : {
        collider : null,

        init : function (config) {
          var property;

          // Polygons is an array of polygons, defined as follows:
          // radius -> radius of the polygon
          // precision -> how many vertices to draw
          // rotation -> rotation of the polygon
          // stretch -> an object containing x, for x stretch and y for y
          // position -> an object containing x/y for offset from parent
          this.polygons = [];

          for (property in config) {
            if (config.hasOwnProperty(property)) {
              this[property] = config[property];
            }
          }
        }
    }
});
