Class(Ogat.Components, "Jump").inherits(Serpentity.Component)({
    prototype : {
        jumps : 1,
        jumpTime : 0,
        jumpLock : false,
        rejumpLock : false,
        multiJumpLock : false,
        jumpStrength: 50,
        currentJumps : 0
    }
});

